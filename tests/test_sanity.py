import pytest
import tictactoe.player
import tictactoe.main
import threading

class Game:
    def __init__( self, symbol, remoteIP ):
        self.symbol = symbol
        self.remoteIP = remoteIP
        self.winner = None
        thread = threading.Thread( target = self.play )
        thread.start()
        self.join = thread.join

    def play( self ):
        self.winner = tictactoe.main.Main( self.symbol, self.remoteIP ).winner

@pytest.fixture
def moves():
    return iter( [ 'a', 'b', 'c', 'd', 'e', 'f', 'g' ] )

@pytest.fixture
def games( moves ):
    tictactoe.player.__builtins__[ 'input' ] = lambda: next( moves )
    gameO = Game( 'O', None )
    gameX = Game( 'X', 'localhost' )
    yield gameO, gameX
    tictactoe.player.__builtins__[ 'input' ] = input

def test_game( games, moves ):
    gameO, gameX = games
    gameO.join()
    gameX.join()
    assert gameO.winner == 'X'
    assert gameX.winner == 'X'
    remainingMoves = list( moves )
    assert len( remainingMoves ) == 0
