import board

class Player( object ):
    def __init__( self, symbol, connection ):
        assert symbol in [ 'X', 'O' ]
        self._symbol = symbol
        self._connection = connection
        self._otherSymbol = { 'X': 'O', 'O': 'X' }[ symbol ]
        self._board = board.Board()
        self._iStart = symbol == 'X'

    def play( self ):
        if self._iStart:
            self._makeMove()
        while not self._board.gameOver():
            self._otherPlayerMoves()
            if self._board.gameOver():
                break
            self._makeMove()
        print( "final board" )
        self._displayBoard()
        print( '%s wins!' % self._board.winner() )

    def _makeMove( self ):
        self._displayBoard()
        print( "%s, it's your turn, make a move." % self._symbol )
        position = input()
        assert position in self._board.POSITIONS
        self._board.put( position, self._symbol )
        self._connection.send( position )

    def _otherPlayerMoves( self ):
        self._displayBoard()
        print( "waiting for %s to move..." % self._otherSymbol )
        position = self._connection.receive()
        self._board.put( position, self._otherSymbol )

    def _displayBoard( self ):
        print( self._board )

    def winner( self ):
        return self._board.winner()
