class Board:
	POSITIONS = 'abcdefghi'

	def __init__( self ):
		self._board = {}
		for x in self.POSITIONS:
				self._board[ x ] = ' '

	def __repr__( self ):
		return  ( '%(a)s|%(b)s|%(c)s\n' +\
				  '-----\n' +\
				  '%(d)s|%(e)s|%(f)s\n' +\
				  '-----\n' +\
				  '%(g)s|%(h)s|%(i)s' ) %\
				self._board

	def put( self, where, what ):
		self._board[ where ] = what

	def winner( self ):
		for player in 'X', 'O':
			if self._won( player ):
				return player
		return None

	def _won( self, player ):
		WINNING_COMBINATIONS = [ 'abc', 'def', 'ghi', 'aei', 'ceg', 'adg', 'beh', 'cfi' ]
		for combination in WINNING_COMBINATIONS:
			if self._allPositionsOccupiedBy( player, combination ):
				return True
		return False

	def _allPositionsOccupiedBy( self, player, positions ):
		playersInPositions = ''.join( [ self._board[ position ] for position in positions ] )
		return playersInPositions.count( player ) == len( playersInPositions )

	def gameOver( self ):
		return self.winner() is not None or self._boardFull()

	def _boardFull( self ):
		return ' ' not in self._board.values()
