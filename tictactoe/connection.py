import socket

PORT = 3333

class Connection( object ):
    def __init__( self, remoteIP  ):
        sock = socket.socket()
        if remoteIP is not None:
            self._client = True
            self._peer = remoteIP, PORT
            sock.connect( self._peer )
            self._connection = sock
        else:
            self._client = False
            sock.bind( ( '', PORT ) )
            sock.listen( 5 )
            self._connection, self._peer = sock.accept()

    def send( self, position ):
        self._connection.send( position.encode( 'latin-1' ) )

    def receive( self ):
        position = self._connection.recv( 1 )
        return position.decode( 'latin-1' )

    def close( self ):
        if self._client:
            self._connection.close()
