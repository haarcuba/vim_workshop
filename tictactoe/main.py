import sys
import player
import connection

class Main( object ):
    def __init__( self, symbol, remoteIP ):
        global connection
        try:
            connection = connection.Connection( remoteIP )
            self._player = player.Player( symbol, connection )
            self._player.play()
            self.winner = self._player.winner()
        finally:
            connection.close()

if __name__ == '__main__':
    if len( sys.argv ) == 1:
        print( "main.py <X|O> [ip of O side]" )
        quit()
    symbol = sys.argv[ 1 ]
    if symbol == 'X':
        remoteIP = sys.argv[ 2 ]
    else:
        remoteIP = None
    Main( symbol, remoteIP )
