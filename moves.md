#short intro
* moving
* gg/G
* easy-move
# subtlety
* w, W, e, E, 0, ^
* f, F, t, T
* the dot-formula
* zz
# yank-put
* y, p, yy, PP
# this-line actions
* dd, yy
#action-motion
* w, dw, cw
* W, dW, cW
* df<x> dt<x> etc
* super lightweight
* ca( da( etc
# splits
* vsplit, split, etc
#edit game
* switch to python 3
    * use macro for print()
    * vimgrep
    * ctags: use on various source trees
    * C-I, C-O
    * `raw_input` -> input ( use df )
* test makeover
    * remove  private variables
    * change tictactoe imports
    * complete localhost

#pytest
* change MOVES to iterator
* two threads, replace O with X
* moves assertion at the end

#knows many many languages

#inc and dec numbers

#run commands
* !xxd
* !sort

#reading from various sources
* read http
* read !ls

#plugins
* git: move, commit (autocomplete), blame
* fzf.vim

#terminal
* save a terminal
* gf - goto file

#also
* learn EMACS shortcuts for shell
